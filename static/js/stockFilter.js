class StockFilter extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          data: [],
          filteredData: [],
          headings: [],
      }
      this.handleChange = this.handleChange.bind(this);
    }
    componentWillMount() {
      $.get('/stocks').done(function(response) {
        var headings = response.shift()
        var data = response
        this.setState({
          data: data,
          headings: headings,
          filteredData: this.defaultFilter(data)
        })
      }.bind(this))
    }
    handleChange(e) {
      var data = this.state.data
      var query = e.target.value.toUpperCase()
      console.log(query)

      if (query) {
        var filteredData = data.filter((row) => {
          return row[0].startsWith(query) || row[1].startsWith(query)
        })
        this.setState({ filteredData: filteredData })
      } else {
        this.setState({ filteredData: this.defaultFilter(data) })
      }
    }
    defaultFilter(data) {
      return data.slice(0, 99)
    }
    render() {
      var data = this.state.filteredData

      return React.createElement('div', {},

        React.createElement('p', {},
          React.createElement('input', {
            onChange: this.handleChange,
            placeholder: 'Search',
            style: {
              'fontSize': 'large',
              'textTransform': 'uppercase'
            },
          })
        ),

        React.createElement('table', {},
          React.createElement('thead', {},
            React.createElement('tr', {},
              this.state.headings.map(function(heading) {
                return React.createElement('th', {
                  key: heading,
                  style: { 'fontWeight': 'bold' },
                }, heading)
              })
            )
          ),
          React.createElement('tbody', {},
            data.map(function(row) {
              return React.createElement('tr', {
                key: row[0]
              },
              row.map(function(cell) {
                return React.createElement('td', {
                  key: cell
                }, cell)
              })
            )
          })
        )
      ))
    }
}
