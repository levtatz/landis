# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse


def data(request):
    file = open('utils/data.txt', 'r')
    lines = [line.rstrip('\n') for line in file]

    # drop first and last lines
    lines.pop(0)
    lines.pop(len(lines)-1)

    table = []
    for line in lines:
        line_arr = line.split('|')

        # keep only wanted columns
        columns_to_keep = [0, 2, 5, 6, 7]
        column_data = []
        for column in columns_to_keep:
            column_data.append(line_arr[column])
        table.append(column_data)

    return JsonResponse(table, safe=False)
