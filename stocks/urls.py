from django.conf.urls import url
from django.views.generic import TemplateView

import views

urlpatterns = [
    url('^stocks$', views.data, name='stocks'),
    url('^$', TemplateView.as_view(template_name='index.html'))
]