## Setup

1. `sudo pip install virtualenv virtualenvwrapper`
2. [Update shell startup file for virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html#shell-startup-file)
3. `mkvirtualenv landis`
4. `git clone git@bitbucket.org:levtatz/landis.git && cd landis`
5. `pip install -r requirements.txt`
7. `./manage.py migrate`
9. `./manage runserver`

Next time, run `workon landis` to load the virtual environment.


## Download data

1. `cd landis`
2. `python utils/ftp.py`
